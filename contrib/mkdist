#!/bin/bash
#
# This script can be used by a cron to generate snapshots.
# For example, use:
# 35 0 * * * mkdist -r elinks-0.11 -l 0.11 -s >>mkdist.log 2>&1
# 40 0 * * * mkdist -r HEAD -l 0.12 -s >>mkdist.log 2>&1
#
# Options:
#   -g GIT_DIR   Git repository from which this script exports ELinks.
#                May be given in the environment instead.
#   -r REVISION  Git revision to be exported from the repository.
#   -l LABEL     User-friendly name of the branch or release.
#                This ends up in the name of the tar file, and in the
#                name of the directory it contains.
#   -s           Generate a snapshot (which has a date in the top-level
#                directory).
#   -d DOCDIR    Copy prebuilt documentation from DOCDIR.
#   -o OUTDIR    Place the output files in OUTDIR.  Defaults to the
#                current directory.

# set -x

echo "-------------------------------------------------"
echo "Date: $(date)"
echo "Args: $*"
echo "-------------------------------------------------"

# Variables used in this script:
# $GIT_DIR     = option -g GIT_DIR; passed in environment to Git
# $OPTARG      = Bash special: argument of the option being parsed
# $OPTIND      = Bash special: index of argument to be parsed next
# $commit      = commit ID corresponding to $rev
# $docdir      = option -d DOCDIR
# $label       = option -l LABEL
# $opt         = option letter being parsed, or '?' on error
# $outdir      = option -o OUTDIR
# $rev         = option -r REVISION
# $snap        = option -s
# $tarbasename = name of the tar file without .tar.* extensions
# $tartopdir   = name of the top directory within the tar file
# $tmpdir      = temporary directory created by this script

rev=
label=
snap=
docdir=
outdir=.
while getopts "g:r:l:sd:o:" opt
do
    case "$opt" in
	(g)	GIT_DIR=$OPTARG ;;
	(r)	rev=$OPTARG ;;
	(l)	label=$OPTARG ;;
	(s)     snap=1 ;;
	(d)	docdir=$OPTARG ;;
	(o)	outdir=$OPTARG ;;
	("?")	exit 1 ;;
	(*)	echo >&2 "$0:$LINENO: bug found"
		exit 1 ;;
    esac
done

if [ $OPTIND -le $# ]
then
    echo >&2 "$0: too many non-option arguments"
    exit 1
fi

if [ -z "$GIT_DIR" ]
then
    echo >&2 "$0: Must specify -g GIT_DIR option"
    exit 1
fi
if [ -z "$outdir" ]
then
    echo >&2 "$0: Must specify -o OUTDIR option"
    exit 1
fi
if [ -z "$rev" ]
then
    echo >&2 "$0: Must specify -r REVISION option"
    exit 1
fi
if [ -z "$label" ]
then
    label=$rev
fi

commit=$(GIT_DIR=$GIT_DIR cg-object-id -c "$rev") || exit 1

if [ "$snap" ]
then
    tartopdir=elinks-$label-$(date +%Y%m%d)
    tarbasename=elinks-current-$label
else
    tartopdir=elinks-$label
    tarbasename=elinks-$label
fi

tmpdir=$(mktemp -d -t elinks-dist-XXXXXXXX) || exit 1

# To make it easier to compare build logs, put the source first in an
# "elinks" directory, and only move to "$tartopdir" when finished.

GIT_DIR=$GIT_DIR cg-export -r "$rev" -- "$tmpdir/elinks"
mkdir -- "$tmpdir/elinks/.git"
printf "%s\n" "$commit" > "$tmpdir/elinks/.git/HEAD"

(set -e
 cd -- "$tmpdir/elinks"
 ./autogen.sh
 mkdir build
 cd build
 ../configure
 make -C po
 mv po/*.gmo ../po/
 mv contrib/elinks.spec ../contrib/
) || exit 1

if [ -n "$docdir" ]; then
	mkdir -- "$tmpdir/elinks/doc/html"
	cp -r -- "$docdir"/*.html* "$tmpdir/elinks/doc/html/"
	# mkdir doc/pdf
	# cp "$docdir"/*.pdf doc/pdf
fi

rm -rf -- "$tmpdir/elinks/build"
mv -- "$tmpdir/elinks" "$tmpdir/$tartopdir"

(set -e
 cd -- "$tmpdir"
 tar cf "$tarbasename.tar" "$tartopdir"
 md5sum --binary -- "$tarbasename.tar" > "$tarbasename.md5"
 bzip2 --keep -- "$tarbasename.tar"
 gzip -9 -- "$tarbasename.tar"
 md5sum --binary -- "$tarbasename.tar.gz" "$tarbasename.tar.bz2" >> "$tarbasename.md5"
) || exit 1
 
mv -- "$tmpdir/$tarbasename.tar.gz"	"$outdir"
mv -- "$tmpdir/$tarbasename.tar.bz2"	"$outdir"
mv -- "$tmpdir/$tarbasename.md5"	"$outdir"
rm -rf -- "$tmpdir"
